import React, { useMemo, useState } from 'react';
import { Demo1 } from './pages/demo1/Demo1';
import { Demo2 } from './pages/demo2/Demo2';
import { Demo3 } from './pages/demo3/Demo3';
import Demo4 from './pages/demo4-use-state/Demo4';
import Demo5 from './pages/demo5-use-state/Demo5';
import { Demo6UseEffect } from './pages/demo6/Demo6UseEffect';
import { Home } from './pages/home/Home';
import { UiKit } from './pages/uikit/UiKit';

type Page =  'home' | 'demo1' | 'demo2' | 'demo3' | 'demo4' | 'demo5' | 'home' | 'uikit' | 'demo6';


function App() {
  const [page, setPage] = useState<Page | null>('demo6')

  const changePage = function (page: Page) {
    setPage(page)
  }

  return (
    <div className="m-3">
      <div>
        <button onClick={() => changePage('home')}>Home</button>
        <button onClick={() => changePage('uikit')}>UiKit(card)</button>
        <button onClick={() => changePage('demo1')}>Demo 1</button>
        <button onClick={() => changePage('demo2')}>Demo 2</button>
        <button onClick={() => changePage('demo3')}>Demo 3</button>
        <button onClick={() => changePage('demo4')}>Demo 4 (state)</button>
        <button onClick={() => changePage('demo5')}>Demo 5 (state)</button>
        <button onClick={() => changePage('demo6')}>Demo 6 (useEffect)</button>
      </div>

      <Page value={page}/>

    </div>
  )
}

export default App;
// =======


interface PageProps {
  value: Page | null;
}
export function Page(props: PageProps) {

  function getPage() {
    switch (props.value) {
      case 'demo1': return <Demo1 />
      case 'demo2': return <Demo2 />
      case 'demo3': return <Demo3 />
      case 'demo4': return <Demo4 />
      case 'demo5': return <Demo5 />
      case 'demo6': return <Demo6UseEffect />
      case 'home': return <Home />
      case 'uikit': return <UiKit />
      default: return <div>Choose a button</div>
    }
  }

  return  <div>{getPage()}</div>
}
