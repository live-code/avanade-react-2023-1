import React from "react";
import { Empty } from './components/Empty';
import { List } from './components/List';
import { Product } from '../../model/product';


const list: Product[] = [
  { id: 1, name: 'Nutella' },
  { id: 2, name: 'Latte' },
];

export function Demo1() {
  return (
    <div>
      {
        list.length > 0 ?
          <List title="LISTA" items={list} /> :
          <Empty />
      }
    </div>
  )
}

