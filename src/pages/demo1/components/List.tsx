import { Product } from '../../../model/product';

export interface ListProps {
  title?: string;
  items: Product[];
}

export function List(props: ListProps) {
  return <div>
    <h1>{props.title}</h1>
    <div>ci sono {props.items.length} prodotti</div>
  </div>
}
