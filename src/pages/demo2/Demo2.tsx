import clsx from 'clsx';

interface User {
  id: number;
  name: string;
  gender: 'M' | 'F'; // literal types
}
const u: User = {
  id: 1,
  name: 'Fabio',
  gender: 'F'
}

const themes = {
  titleBar: {
    color: '#ff0000'
  }
}

export function Demo2() {
  return <div>
    Demo2

    <h1
      style={themes.titleBar}
      className={clsx(
        'titleBar',
        { 'male': u.gender === 'M' },
        { 'female': u.gender === 'F' },
      )}
    >{u.name}</h1>

    <button className="btn btn-primary">
      CLICK
      <i className="fa fa-times"></i>
    </button>

    <div className="card" style={{ width: "18rem" }}>
      <img src="..." className="card-img-top" alt="..." />
      <div className="card-body">
        <h5 className="card-title">Card title</h5>
        <p className="card-text">
          Some quick example text to build on the card title and make up the bulk of
          the card's content.
        </p>
        <a href="#" className="btn btn-primary">
          Go somewhere
        </a>
      </div>
    </div>


  </div>
}



// utils.ts
