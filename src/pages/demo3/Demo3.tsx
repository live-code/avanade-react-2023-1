import React from 'react';

export function Demo3() {
  const name = 123;

  console.log('demo3')
  function gotoUrl(url: string) {
    window.open(url)
  }

  function doSomething(e: React.MouseEvent, url: string) {
    console.log(e.clientX)
    console.log(url)
  }

  function keydownHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      window.open(`https://en.wikipedia.org/wiki/${e.currentTarget.value}`)
    }
  }

  return <div>
    <h1>Demo3</h1>
    <button onClick={(e) => doSomething(e, 'http://www.google.com')}>CLICK ME 1</button>
    <button onClick={(e) => doSomething(e, 'http://www.fabiobiondi.dev')}>CLICK ME 1</button>

    <button onClick={() => gotoUrl('https://fabiobiondi.dev')}>FB</button>
    <button onClick={() => gotoUrl('https://google.dev')}>Google</button>

    <input type="text" onKeyDown={keydownHandler} />
  </div>
}

