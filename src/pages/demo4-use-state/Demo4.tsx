import { useEffect, useState } from 'react';
import { Card } from '../../uikit/Card';
// hooks

export default function Demo4 () {
  const [count, setCount] = useState<number>(0)


  function inc() {
    setCount(s => s + 1)
  }

  const invalid = count < 3;

  return <div>
    { invalid && <div>count must be greater then 3</div>}
    <h1>Demo4</h1>
    <button onClick={inc}>{count}</button>

    <button disabled={invalid}>SEND</button>
  </div>
};


function inc(e: MouseEvent) {

}

addEventListener('click', inc)
