import { useEffect, useState } from 'react';
import { Card } from '../../uikit/Card';

interface CounterState {
  value: number;
  invalid: boolean;
  name: string;
}

export default function Demo5 () {
  const [count, setCount] = useState<CounterState>({ name:'fab', value: 0, invalid: true })
  const [items, setItems] = useState<number[]>([1, 2, 3])

  function inc() {
    setCount((s) => ({
      ...s,
      value: s.value + 1,
      invalid: s.value < 3,
    }))

    setItems(s => [...s, Math.random()])
  }

  function removeItem(itemToRemove: number) {
    console.log(itemToRemove)
    setItems(s => s.filter(item => item !== itemToRemove) )
  }

  return <div>
    <h1>Demo5</h1>

    <ul>
      {
        items.map(item => {
          return  <li onClick={() => removeItem(item)}>{item}</li>
        })

      }
    </ul>


    <button onClick={inc}>{count.value}</button>
    <button onClick={() => setItems([])}>clean</button>

    <button disabled={count.invalid}>SEND</button>
  </div>
};


function inc(e: MouseEvent) {

}

addEventListener('click', inc)
