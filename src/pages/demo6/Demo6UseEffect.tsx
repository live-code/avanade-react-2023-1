import axios from 'axios';
import { useEffect, useState } from 'react';
import { User } from '../../model/user'; // hooks

export function Demo6UseEffect() {
  const [users, setUsers] = useState<User[]>([])

  useEffect(() => {
    getUsers()
  }, [])

  function getUsers() {
    axios.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .then((response) => {
        setUsers(response.data)
      })
      .catch((error) => {
        console.log('Error:', error);
      });
  }

  console.log('render', users)

  function removeUser(idToRemove: number) {
    axios.delete(`https://jsonplaceholder.typicode.com/users/${idToRemove}`)
      .then(() => {
        const newUsers = users.filter(user => user.id !== idToRemove)
        setUsers(newUsers)
      })
  }

  return <h1>
    demo 6

    <div>
      {
        users.map((user) => {
          return <li key={user.id}>
            {user.name}
            <i className="fa fa-trash" onClick={() => removeUser(user.id)}></i>
          </li>
        })
      }

    </div>

  </h1>
}
