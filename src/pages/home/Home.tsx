import { Jumbotron } from './components/Jumbotron';
import { LeftCol } from './components/LeftCol';
import { NavBar } from './components/NavBar';
import { RightCol } from './components/RightCol';


const myTitle = 'title 2'
export function Home() {
  return <div>
    <main>
      <div className="container py-4">
        <NavBar />

        <Jumbotron
          icon="fa fa-linkedin"
          title="My title"
          description="bla bla"
          button={{
            label: "visit",
            url: 'https://www.fabiobiondi.dev'
          }}
        />

        <Jumbotron
          theme="dark"
          title={'hello ' } description="lorem ipsum"
          button={{
            label: "visit",
            url: 'https://www.fabiobiondi.dev'
          }}
        />


        <div className="row align-items-md-stretch">
          <div className="col-md-6">
            <RightCol />
          </div>
          <div className="col-md-6">
            <LeftCol />
          </div>
        </div>



        <footer className="pt-3 mt-4 text-muted border-top">© 2022</footer>
      </div>
    </main>

  </div>
}
