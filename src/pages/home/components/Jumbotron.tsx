import clsx from 'clsx';

interface JumbotronProps {
  title: string;
  icon?: string;
  description: string;
  button: {
    label: string;
    url: string;
    target?: '_self' | '_blank';
  }
  theme?: 'dark' | 'light'
}


export function Jumbotron(props: JumbotronProps) {
  const {
    theme = 'light',
    icon,
    button: {
      label,
      url,
      target = 'blank'
    },
    title,
    description,
  } = props;


  return (
    <div className={clsx(
      'p-5 mb-4  rounded-3',
      {'bg-dark text-white': theme === 'dark'},
      {'bg-light': theme === 'light'},
    )}>
      <div className="container-fluid py-5">
        <h1 className="display-5 fw-bold">
          { icon && <i className={icon} />}

          {title}
        </h1>
        <p className="col-md-8 fs-4">
          {description}
        </p>
        {
          label &&
          <a
            href={url}
            target={target}
            className="btn btn-primary btn-lg" type="button"
          >
            {label}
          </a>
        }
      </div>
    </div>
  )
}
