import { Card } from '../../uikit/Card';


export function UiKit() {

  function openUrl(val: string) {
    window.open(val)
  }

  return <div>
    <Card
      title="Fabio Biondi"
      icon="fa fa-linkedin"
      onIconClick={() => openUrl('http://www.linkedin.com')}
    >
      content...
    </Card>

    <br/>
    <Card
      title="Mario Rossi"
      bodyCls="bg-dark text-white"
      primary
      icon="fa fa-google"
      onIconClick={(e: React.MouseEvent) => {
        openUrl('http://www.google.com')
      }}
    >
      content
    </Card>

  </div>
}
