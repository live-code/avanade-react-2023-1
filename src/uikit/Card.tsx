import clsx from 'clsx';
import React, { PropsWithChildren } from 'react';

export type CardProps = {
  title: string;
  primary?: boolean;
  secondary?: boolean;
  bodyCls?: string;
  icon?: string;
  onIconClick?: (e: React.MouseEvent) => void;
}

export function Card(props: PropsWithChildren<CardProps>) {
  const {
    title, children, bodyCls, icon, primary, secondary
  } = props;
console.log('card: render')
  return (
    <div className="card">
      <div
        className={clsx('card-header', {
          'bg-info': primary,
          'bg-secondary': secondary,
        }) }
      >
        <div className="d-flex justify-content-between align-items-center">
          <div>{title}</div>
          {
            icon &&
              <i className={icon}
                 onClick={props.onIconClick}></i>
          }
        </div>
      </div>
      <div className={clsx('card-body', bodyCls) }>
        {children}
      </div>
    </div>
  )
}
