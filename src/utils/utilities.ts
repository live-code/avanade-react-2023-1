export function add(a: number, b: number) {
  return a + b;
}

export default function divide() {
}


export function getCls(gender: 'M' | 'F') {
  switch(gender) {
    case 'M': return 'male'
    case 'F': return 'female'
    default:
      return '...'
  }
}
